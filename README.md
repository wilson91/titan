# LIGO_AUX_Chan_MLA
Kalista's Honors Thesis Code

The data in its most useful format exists on elrond under the dreictory: /home/wayt1/subsystem_data. Go into the directory and navigate to the set of data you want to use. The unseen_test_data is the ten time seperated test chunks, whereas the data living directly in the directory composes the dataset. Chunkall holds the data used for the training of TITAN, Hyper-GIANTS, and GIANTS. 

The dataset in their raw unprocessed form sits on elrond.
Those files that exist in /home/wayt1/copied_data is the training datasets directly taken from iDQ. The ones used in this project are just the whitened_train numpy arrays and the train labels. 

Those files that exist in /home/wayt1/copied_unseen_test_data  is the unseen test datasets directly taken from iDQ. The ones used in this project are just the whitened_train numpy arrays and the train labels. 

In /home/wayt1/data_built the data sits in the columns are arragned alphabetically, or aligned, but not yet broken down by training group. In /home/wayt1/data_built/train1 and /home/wayt1/data_built/train2 and /home/wayt1/data_built/test we get the broken down data sets by training group and test group. In these directories also sits the combined chunks into one massive data set. It is at this point you will have memory problems if you recombine train1 and train2 (all I can say is code smart and good luck). 

In /home/wayt1/data_built/unseen_test_data you will find the time seperated datasets in the combined form. Note that this does not need to be broken up into training groups or test groups so there is no second level directories. 

The final thing to note about the data sets, at some point in time the data switched from being broken down by day to just being passed as a week. If you are looking for the data sets within an individual day navigate to /home/wayt1/copied_data or /home/wayt1/copied_unseen_test_data and go the the chunk and day you are intreseted in. The aligned column days exist in these directories. The days being added to make weeks are then in /home/wayt1/data_built. 

These are all locations where all the data sets are, to see the code that made the data sets explore making data folder. 


To find the hyperparameter tuning go onto ver-rubin and go to /home/kalista.wayt/hypertuning_subsystem then run the python script with the corresponding config file in each subsystem folder. Finally note that the result of hyperparamter runs sit in these folders on vera-rubin in .txt files corresponding the the run parameters and were simply to large to add here. 


Scattered light glitch datasets sit on elrond at /home/wayt1/scattered_light_glitch_data_sets
