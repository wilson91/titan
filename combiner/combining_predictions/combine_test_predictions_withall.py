import numpy as np

#---------------------------------
subsystem_predictions=np.load("/home/kalista.wayt/predictions_data_combined/hypertuned/chunkall_balanced_1layer_test_20patience.npy")
print("Subsystem shape", subsystem_predictions.shape)
all_predictions=np.loadtxt("/home/kalista.wayt/public_html/ANN_ALL/chunkall_savemodel_unbalanced_1layer_testpredictions_balanced/predictions.txt")
print("Subsystem shape", all_predictions.shape)
predictions=np.column_stack((subsystem_predictions,all_predictions))
print("Shape of predictions:", predictions.shape)
print(predictions)
print("Shape of Predcitions should be (1000,13):",predictions.shape)
np.save("/home/kalista.wayt/predictions_data_combined/hypertuned/chunkall_balanced_1layer_test_20patience_balance_unbalanced_train1all_model.npy",predictions)


