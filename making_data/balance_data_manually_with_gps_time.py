import numpy as np

#---------------------------
chunks=['05','06','07','08','09','10']
subsystem=["ASC","CAL","HPI","IMC","ISI","LSC","OMC","PEM","PSL","SQZ","SUS","TCS"]
for i in chunks:
    print("chunk{}".format(i))
    for j in subsystem:
        print("Subsystem={}".format(j))
        data_CAL=np.load("/home/wayt1/subsystem_data/unseen_test_data/chunk{}/whitened_train_features_H1:CAL.npy".format(i))
        data=np.load("/home/wayt1/subsystem_data/unseen_test_data/chunk{}/whitened_train_features_H1:{}.npy".format(i,j))
        print("Data shape before",data.shape)
        assert len(data_CAL)==len(data)
        labels=np.load("/home/wayt1/data_built/unseen_test_data/labels_chunk{}.npy".format(i))
        labels_cal=np.load("/home/wayt1/subsystem_data/unseen_test_data/chunk{}/labels_chunk{}.npy".format(i,i))
        
        gps_times=np.load("/home/wayt1/copied_unseen_test_data/test_data/set{}/train_times.npy".format(i))
        
        assert len(labels_cal)==len(labels)
        assert len(labels_cal)==len(gps_times)
        print("Sainty checks passed")

        gps_time_added_data=np.column_stack((data,gps_times))
        data_shape=data.shape
        print("Data shape",data_shape)
        data_shape_plus_gps=gps_time_added_data.shape
        print("Data shape +gps time",data_shape_plus_gps)

        assert data_shape[1]==(data_shape_plus_gps[1]-1)

        print("All assertions have been passed")
        sorted_indxs = np.argsort(labels, axis = None)
        data= gps_time_added_data[sorted_indxs][::-1]
        labels = labels[sorted_indxs][::-1]
      # First sort the data and organize it so that the glitches come first and cleans come last
        unclean=int(sum(labels))
        clean = int(len(data) - unclean)
        print(f"Length of Data before Balancing:{len(data)}, Glitch:{unclean}, Clean:{clean}")

        data_glitch=data[:unclean,:]
        label_glitch=labels[:unclean]

        data_clean=data[unclean:,:]
        label_clean=labels[unclean:]

        ratio=int(clean/unclean)
        data_clean = data_clean[::ratio]
        #print(data)
        label_clean = label_clean[::ratio]    #slice as many labels as data

        data=np.concatenate((data_glitch,data_clean),axis=0)
        labels=np.concatenate((label_glitch,label_clean),axis=0)

        #print("Labels after being sliced:",labels)
        # Count number of glitch and clean again
        unclean = int(labels.sum())
        clean = len(data) - unclean
        print(f"Length of Data after Balancing:{len(data)}, Glitch:{unclean}, Clean:{clean}")
       
        print("Data shape balanced", data.shape)

        np.save("/home/wayt1/subsystem_data/unseen_test_data/chunk{}/gps_time_data/whitened_train_features_H1:{}_balanced_with_gps_time.npy".format(i,j),data)

        gps_times_balanced=data[:,data_shape[1]]
        data_gps_pulled_off=data[:,0:data_shape[1]]

        assert data_shape[1]==data_gps_pulled_off.shape[1]
        assert len(data_gps_pulled_off)==len(gps_times_balanced)

        print("Final asserations have been passed. Balanced data set along with corresponding gps times exist")

        np.save("/home/wayt1/subsystem_data/unseen_test_data/chunk{}/gps_time_data/whitened_train_features_H1:{}_balanced.npy".format(i,j),data_gps_pulled_off)
    np.save("/home/wayt1/subsystem_data/unseen_test_data/chunk{}/gps_time_data/labels_balanced.npy".format(i),labels)
