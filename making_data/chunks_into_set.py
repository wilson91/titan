import numpy as np

#-------------------------------------

chunk01=np.load("/home/wayt1/data_built/whitened_train_features_chunk01.npy")
labels01=np.load("/home/wayt1/data_built/labels_chunk01.npy")
chunk18=np.load("/home/wayt1/data_built/whitened_train_features_chunk18.npy")
labels18=np.load("/home/wayt1/data_built/labels_chunk18.npy")
chunk24=np.load("/home/wayt1/data_built/whitened_train_features_chunk24.npy")
labels24=np.load("/home/wayt1/data_built/labels_chunk24.npy")
chunk37=np.load("/home/wayt1/data_built/whitened_train_features_chunk24.npy")
labels37=np.load("/home/wayt1/data_built/labels_chunk37.npy")

data=np.concatenate((chunk01,chunk18),axis=0)
data=np.concatenate((data,chunk24),axis=0)
data=np.concatenate((data,chunk37),axis=0)
labels=np.append(labels01,labels18)
labels=np.append(labels,labels24)
labels=np.append(labels,labels37)


print("Shape of data:", data.shape)
print("Length of labels:", len(labels))
print("Number of glitches:", sum(labels))
print("Number of cleans:",len(labels)-sum(labels))

np.save("/home/wayt1/data_built/whitened_train_features_chunkall.npy",data)
np.save("/home/wayt1/data_built/labels_chunkall.npy",labels)
