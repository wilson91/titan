import numpy as np
import pandas as pd
#----------------------------------------------
chunks=['01','18','24','37']
days=['01','02','03','04','05','06','07']
sub_system=["H1:ASC","H1:CAL","H1:HPI","H1:IMC","H1:ISI","H1:LSC","H1:OMC","H1:PEM","H1:PSL","H1:SQZ","H1:SUS","H1:TCS"]
for i in chunks:
    for k in days: 
        print("Chunk{} Day{}".format(i,k))
        df=pd.read_csv("/home/wayt1/copied_data/chunk{}/day{}/dataframe_chunk{}.csv".format(i,k,i))
        column_names=df.columns
        print("Number of columns:", len(df.columns))
        na_or_0_cols = []
        for column in df:  # iterates by-name
            if df[column].isna().all() or (df[column] == 0).all():
                na_or_0_cols.append(column)
        print(na_or_0_cols)
        print("Length of zero columns:", len(na_or_0_cols))
        print("Length of all columns:", len(column_names))
        if len(column_names) == len(na_or_0_cols):
            print("This day has only zeros:")
        else:
            pass
        for j in sub_system:
            
            l=0
            yesno_zero=[]
            while l<len(na_or_0_cols): 
                yesno_zero.append(na_or_0_cols[l].startswith(j))
                l+=1
            l=0  
            yesno_all=[]
            while l<len(column_names): 
                yesno_all.append(column_names[l].startswith(j))
                l+=1
    
            zero_amount=sum(yesno_zero)
            total_amount=sum(yesno_all)
            
            if zero_amount == total_amount: 
                print("{} on this day has only zeros:".format(j))
            else:
                print("{} on this day has this many none zero columns also number of total columns".format(j),total_amount-zero_amount, total_amount)
            


