import numpy as np

#----------------------------------
chunks=['01','02','03']
glitch_in_test=0
clean_in_test=0

for i in chunks:
    print("chunk{}".format(i))
    input_path_data="/home/wayt1/data_built/unseen_test_data/whitened_train_features_chunk{}.npy".format(i)
    input_path_labels="/home/wayt1/data_built/unseen_test_data/labels_chunk{}.npy".format(i)
    save_path_train1_data="/home/wayt1/data_built/unseen_test_data/whitened_train_features_80per_train_chunk{}.npy".format(i)
    save_path_train2_data="/home/wayt1/data_built/unseen_test_data/whitened_train_features_20per_train_chunk{}.npy".format(i)
    save_path_test_data="/home/wayt1/data_built/"
    save_path_train1_labels="/home/wayt1/data_built/unseen_test_data/labels_80per_train_chunk{}.npy".format(i)
    save_path_train2_labels="/home/wayt1/data_built/unseen_test_data/labels_20per_train_chunk{}.npy".format(i)
    save_path_test_labels="/home/wayt1/data_built/"

    
    
    data=np.load(input_path_data)
    labels=np.load(input_path_labels)

    sorted_indxs = np.argsort(labels, axis = None)
    data= data[sorted_indxs][::-1]
    labels = labels[sorted_indxs][::-1]

    print("Shape of data:", data.shape)
    print("Length of labels:", len(labels))
    print("Number of glitches:", sum(labels))
    print("Number of cleans:",len(labels)-sum(labels))

    num_glitch=int(sum(labels))
    num_clean=int(len(labels)-sum(labels))
    
    if glitch_in_test==0:
        data_glitch=data[:num_glitch,:]
        label_glitch=labels[:num_glitch]

        data_clean=data[num_glitch:,:]
        label_clean=labels[num_glitch:]
        
        num_glitch_train2=int(num_glitch*.2)
        num_clean_train2=int(num_clean*.2)

        train2_glitch_ind=np.random.choice(len(data_glitch)-1,num_glitch_train2,replace=False)
        train2_clean_ind=np.random.choice(len(data_clean)-1,num_clean_train2,replace=False)

        train2_g=np.take(data_glitch,train2_glitch_ind,axis=0)
        labels2_g=np.take(label_glitch,train2_glitch_ind,axis=0)

        train1_g=np.delete(data_glitch,train2_glitch_ind,axis=0)
        labels1_g=np.delete(label_glitch,train2_glitch_ind,axis=0)

        train2_c=np.take(data_clean,train2_clean_ind,axis=0)
        labels2_c=np.take(label_clean,train2_clean_ind,axis=0)

        train1_c=np.delete(data_clean,train2_clean_ind,axis=0)
        labels1_c=np.delete(label_clean,train2_clean_ind,axis=0)

        train1=np.concatenate((train1_g,train1_c),axis=0)
        labels1=np.concatenate((labels1_g,labels1_c),axis=0)

        train2=np.concatenate((train2_g,train2_c),axis=0)
        labels2=np.concatenate((labels2_g,labels2_c),axis=0)

        print("Shape of train1:", train1.shape)
        print("Length of labels1:", len(labels1))
        print("Number of glitches 1:", sum(labels1))
        print("Number of cleans 1:",len(labels1)-sum(labels1))
        print("Shape of train2:", train2.shape)
        print("Length of labels2:", len(labels2))
        print("Number of glitches 2:", sum(labels2))
        print("Number of cleans 2:",len(labels2)-sum(labels2))

        np.save(save_path_train1_data,train1)
        np.save(save_path_train1_labels,labels1)
        np.save(save_path_train2_data,train2)
        np.save(save_path_train2_labels, labels2)                                                                                               

    else:    
        data_glitch=data[:num_glitch,:]
        label_glitch=labels[:num_glitch]

        data_clean=data[num_glitch:,:]
        label_clean=labels[num_glitch:]
    
        test_glitch_ind=np.random.choice(num_glitch,glitch_in_test,replace=False)
        test_clean_ind=np.random.choice(num_clean,clean_in_test,replace=False)
        #This is what this does: NP.take(array_to_take_from, indicies_to_take) 
    
        test_g=np.take(data_glitch,test_glitch_ind,axis=0)
        labelste_g=np.take(label_glitch,test_glitch_ind,axis=0)
        #NP.delete(array_to_delete_from, indicies_to_delte)
        train_g=np.delete(data_glitch,test_glitch_ind,axis=0)
        labelstr_g=np.delete(label_glitch,test_glitch_ind,axis=0)
    
        test_c=np.take(data_clean,test_clean_ind,axis=0)
        labelste_c=np.take(label_clean,test_clean_ind,axis=0)
        train_c=np.delete(data_clean,test_clean_ind,axis=0)
        labelstr_c=np.delete(label_clean,test_clean_ind,axis=0)


        test=np.concatenate((test_g,test_c),axis=0)
        labelste=np.concatenate((labelste_g,labelste_c),axis=0)

        num_glitch_train2=int(sum(labelstr_g)*.2)
        num_clean_train2=int(len(labelstr_c)*.2)
    
        train2_glitch_ind=np.random.choice(len(train_g)-1,num_glitch_train2,replace=False)
        train2_clean_ind=np.random.choice(len(train_c)-1,num_clean_train2,replace=False)

    
        train2_g=np.take(train_g,train2_glitch_ind,axis=0)
        labels2_g=np.take(labelstr_g,train2_glitch_ind,axis=0)

        train1_g=np.delete(train_g,train2_glitch_ind,axis=0)
        labels1_g=np.delete(labelstr_g,train2_glitch_ind,axis=0)

        train2_c=np.take(train_c,train2_clean_ind,axis=0)
        labels2_c=np.take(labelstr_c,train2_clean_ind,axis=0)
    
        train1_c=np.delete(train_c,train2_clean_ind,axis=0)
        labels1_c=np.delete(labelstr_c,train2_clean_ind,axis=0)

        train1=np.concatenate((train1_g,train1_c),axis=0)
        labels1=np.concatenate((labels1_g,labels1_c),axis=0)

        train2=np.concatenate((train2_g,train2_c),axis=0)
        labels2=np.concatenate((labels2_g,labels2_c),axis=0)

        print("Shape of train1:", train1.shape)
        print("Length of labels1:", len(labels1))
        print("Number of glitches 1:", sum(labels1))
        print("Number of cleans 1:",len(labels1)-sum(labels1))
        print("Shape of train2:", train2.shape)
        print("Length of labels2:", len(labels2))
        print("Number of glitches 2:", sum(labels2))
        print("Number of cleans 2:",len(labels2)-sum(labels2))
        print("Shape of test:", test.shape)
        print("Length of test:", len(labelste))
        print("Number of glitches test:", sum(labelste))
        print("Number of cleans test:",len(labelste)-sum(labelste))

        np.save(save_path_train1_data,train1)
        np.save(save_path_train1_labels,labels1)
        np.save(save_path_train2_data,train2)
        np.save(save_path_train2_labels, labels2)
        np.save(save_path_test_data,test)
        np.save(save_path_test_labels,labelste)
